package chapter9

/**
 * Created by grkim on 2015-08-05.
 */
object c9_4NewControlStroucture {
   def main(args: Array[String]) {

      val plusTen = (_:Double) + 10
      val divide5 = (_:Double) / 5

      println( twice(plusTen, 10) )

      println( twice( divide5, 10) )

   }

   def twice(operation:Double => Double, x:Double) = operation(x)


}
