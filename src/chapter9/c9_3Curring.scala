package chapter9

/**
 * Created by grkim on 2015-07-27.
 */
object c9_3Curring {

   def curriedSum(x:Int)(y:Int) = x + y

   def main (args: Array[String]) {
      println( curriedSum(1)(2) )

      val plusOne = curriedSum(x = 1)_

      println( plusOne(10))

   }


}
