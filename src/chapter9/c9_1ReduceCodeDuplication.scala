package chapter9

/**
 * Created by grkim on 2015-07-27.
 */
object c9_1ReduceCodeDuplication {

   private def filesHere = (new java.io.File(".")).listFiles

   def filesEnding(query: String) =
      for (file <- filesHere; if file.getName.endsWith(query))
         yield file

   def fileContains(query:String) =
      for(file<-filesHere; if file.getName.contains(query))
         yield file

   def fielsRegex(query:String) =
      for(file<-filesHere; if file.getName.matches(query))
            yield file


   def fileMatching(query:String, matcher: (String, String) => Boolean ) = {
      for(file <- filesHere; if matcher(file.getName, query))
         yield file

   }

   def containsNeg(nums:List[Int]): Boolean = {
      var exists  = false
      for (num<-nums)
         if(num<0)
            exists = true
      exists
   }

   def containsNeg2(nums:List[Int]) = nums.exists(_ < 0)


   def main(args: Array[String]) {
      val a = fileMatching(query = "a", matcher = _.contains(_) )

      for ( file <- a ) println( file )

      println(containsNeg(List(1, 2, 3)))
      println(containsNeg2(List(1, 2, 3, -1)))

   }



}
