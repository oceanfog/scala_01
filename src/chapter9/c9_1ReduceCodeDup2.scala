package chapter9

/**
 * Created by grkim on 2015-07-29.
 */
object c9_1ReduceCodeDup2 {
   private val someNumbers = List(-15, -10, -5, 0, 5, 10, 15)

   def main(args: Array[String]) {

      for( num <- numberBigger(10)) println("bigger:" + num )

      for( num <- numberShorter(-5)) println("shorter:" + num )

      for( num <- numberSame(-5)) println("same:" + num )


      val a = (x:Int) => x + 10
      println("a:" + a(10) )

      val b = (x:Int, y:Int) => x + y
      println("b:" + b(20, 30))

      val st_plus = (_:Int) + (_:Int)
      val st_minus = (_:Int) - (_:Int)

      println( "getResult_plus:" + getResult(st_plus, 10, 20))
      println( "getResult_minus:" + getResult(st_minus, 10, 20))

   }

   def getResult( statement:(Int, Int) => Int, a:Int, b:Int ) = statement(a, b)

   def numberBigger(query:Int) =
      for(number <- someNumbers; if number > query)
         yield number

   def numberShorter(query:Int) =
      for(number <- someNumbers; if number < query)
         yield number

   def numberSame(query:Int) =
      for(number <- someNumbers; if number == query)
         yield number



}
