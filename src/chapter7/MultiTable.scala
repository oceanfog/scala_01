package chapter7

/**
 * Created by grkim on 2015-07-15.
 */
class MultiTable(r:Int, c:Int) {
   require(r != 0)
   require(c != 0)

   val rows = r
   val columns = c

   def this() = this(10, 10)  //sub constructor



}
