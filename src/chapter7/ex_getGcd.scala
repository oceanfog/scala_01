package chapter7

/**
 * Created by grkim on 2015-07-10.
 */
object ex_getGcd {

  def main(args: Array[String]) {

    println(getGcd(6, 9))

    println(gcd(6, 9))

    println(gcd(12378, 3054))

  }

  def getGcd(x:Long, y:Long): Long ={
    var a = x; //6
    var b = y; //9

    while(a != 0){
      val temp = a //x로 받은 값(6)을 넣음
      a = b % a //b(9)를 a로 나눈 나머지를 a에 넣는다.
      b = temp //b에 temp의 값(3)을 넣는다.
    }

    b //b를 return함

   }

  def gcd(x:Long, y:Long):Long
    = if(y==0) x  //나머지가 0이면 x를 return
      else gcd(y, x%y)  //x mod y(로 나눈 나머지>를 2nd parameter로 넘김



}
