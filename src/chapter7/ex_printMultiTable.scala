package chapter7

/**
 * Created by grkim on 2015-07-15.
 */
/*
 1*1 1*2 1*3 1*4 ···· 1*10
 2*1 2*2 2*3 2*4 ···· 2*10
 ···
 ···
 10*1  10*2  10*3  ··· 10*10

 7.9 generate New Collection
 yield(예일드): 넘겨주다, 양도하다

  */
object ex_printMultiTable {
   def main(args: Array[String]) {
      //printMultiTable()

      val a = makeRowSeq(1).mkString
      print(a)

   }

   def makeRowSeq(row: Int) =
      for(col <- 1 to 10) yield{
         val prod = (row * col).toString
         val padding = " " * ( 4 - prod.length)
         padding + prod
      }

   def getPadding(length:Int):String = " " * ( 4 - length )

  def printMultiTable(): Unit = {
    for( i <- 1 to 10 ){
       for( j <- 1 to 10 ){
          val prod:String = (i * j).toString
          print( getPadding(prod.length) + prod )

        }
        println()

    }

  }

}
