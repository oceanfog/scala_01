package chapter7

/**
 * Created by grkim on 2015-07-10.
 */


// loop and expression

object ex_for {
  def main(args: Array[String]) {
    val filesHere = (new java.io.File("c:/")).listFiles

    for(file <- filesHere) println(file)


    //for(i:Int <- 1 to 100 if i % 2 != 0) println(i) //이 연산의 결과를 알고 싶다.

    //val folders = sFiles(filesHere) //array[Long]

    //for(folder <- folders)println(folder)

    println(getEvenNumArray(1, 10))


  }

  def sFiles(files:Array[java.io.File]):Array[Long] =
    for{
      file <- files
      if file.getName.endsWith("s")

    } yield file.length


  def getEvenNumArray(start_num:Int, end_num:Int) =
    for( i <- start_num to end_num if i % 2 == 0 ) yield{
      i
    }



}
