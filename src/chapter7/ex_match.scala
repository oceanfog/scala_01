package chapter7

/**
 * Created by grkim on 2015-07-15.
 */
object ex_match {

  def main(args: Array[String]) {
    val firstArg = "salt"

    val friend = getFriend(firstArg)

    println(friend)
  }

  def getFriend(firstArg:String):String =
    firstArg match{
    case "salt" => "pepper"
    case "chips" => "salsa"
    case "eggs" => "bacon"
    case _ => "huh?"
  }


}
