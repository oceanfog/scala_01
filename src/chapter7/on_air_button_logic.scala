package chapter7

/**
 * Created by grkim on 2015-07-10.
 */
object on_air_button_logic {

  def main(args: Array[String]) {
    var gstatus = 3

    val button_blind:String = if(gstatus == 1) "blind"
      else if(gstatus == 3) "cancel"
      else ""

    println("button_status: "+button_blind)

  }


}
