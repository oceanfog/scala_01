/**
 * Created by grkim on 2015-06-15.
 */
object obj_PracticeArray {

  def main (args: Array[String]) {

    val teams = Array("SS", "WO", "NC", "LG", "SK"
      ,"OB", "LT", "HT", "HH", "KT")

    for(team <- teams){
      println(team)
    }

  }
}
