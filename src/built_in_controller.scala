/**
 * Created by grkim on 2015-07-09.
 */
object built_in_controller {

  def main(args: Array[String]) {

    println(gcdLoop(4, 8))

  }

  def gcdLoop(x:Long, y:Long):Long = {
    var a = x
    var b = y

    while (a != 0){
      val temp = a
      a = b%a
      b = temp
    }

    b
  }

}
