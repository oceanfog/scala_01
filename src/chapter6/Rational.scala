package chapter6

/**
 * Created by me on 2015-07-22.
 */
class Rational(n:Int, x:Int) {
   val number = n
   val number2 = x

   def this() = this(20, 30)
   def this(n:Int) = this(n, 10)

   def getPlus(n:Int, x:Int) = println(n + x)
   def gePlus(n:Int) = println(n + 10)
   def gePlus(l:String) = println(l+50)

   override def toString = number + "/" + number2


}
