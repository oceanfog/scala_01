package chapter6

/**
 * Created by me on 2015-07-22.
 */
object c6_hello {
   def main(args: Array[String]) {
      println("hello")

      getPlus(10, 20)

      val r = new Rational(10, 20)

      println(r.number)
      println(r.number2)

      r.gePlus(30)
      r.getPlus(20, 30)

      println(r)

      val r3 = new Rational();

      r.gePlus("hello")






   }

   def getPlus(n:Int, x:Int) = println(n + x)
}
