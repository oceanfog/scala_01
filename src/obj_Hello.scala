/**
 * Created by grkim on 2015-06-10.
 */
object obj_Hello {


  def main (args: Array[String]) {
    println("hello world");

    val rational01 = new Rational(1,5)
    val rational02 = new Rational(1,6)

    println(rational01 add rational02)

  }

  def max(x:Int, y:Int):Int = {
    if (x > y) x
    else y
  }



}
