package chapter8

/**
 * Created by grkim on 2015-07-22.
 */
object c8_9Recursion {


   def main(args: Array[String]) {

      val a = getPlus(5)
      println(a)

   }

   def getPlus(n:Int):Int =
      if(n < 10) getPlus( n + 1)
      else n

}
