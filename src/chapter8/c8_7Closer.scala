package chapter8

/**
 * Created by grkim on 2015-07-17.
 */
object c8_7Closer {

   def main(args: Array[String]) {
      //val more = 1
      //print((x:Int)=> x + more)

      val someNumbers = List(-11, -10, -5, 0, 5, 10)
      var sum = 0

      someNumbers.foreach(sum += _)


      val a = makeIncreaser(9999)
      val b = makeIncreaser(999)

      println(a(1))
      println(b(2))



   }

   def makeIncreaser(more:Int) = (x:Int) => x + more

}
