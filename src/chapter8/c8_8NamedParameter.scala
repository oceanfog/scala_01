package chapter8

/**
 * Created by grkim on 2015-07-17.
 */
object c8_8NamedParameter {
   def main(args: Array[String]) {

      printHello(msg = "hello")
   }

   def printHello(msg:String, name:String = "kyeongrok") = println(msg + " " + name + "!")

}
