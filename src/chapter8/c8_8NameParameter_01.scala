package chapter8

/**
 * Created by me on 2015-07-29.
 */
object c8_8NameParameter_01 {
   def main(args: Array[String]) {
      println("hello")

      println("시속:" +  getSpeed( distance = 200  ) + "km/h")

   }

   def getSpeed(time:Int = 1, distance:Float ) = distance / time


}
