package chapter8

/**
 * Created by grkim on 2015-07-15.
 */
object c8_3Literal {
   def main(args: Array[String]) {
      var increase = (x:Int, y:Int)=> x + y

      println(increase(10, 20))

      val someNumbers = List(-11, -10, -5, 0, 5, 10)
      someNumbers.foreach((x:Int) => println(x))

      val newNumbers = someNumbers.filter((x:Int)=>x>0)

      for(a <- newNumbers)println(a)


      val calc = getCalculator()
      println("calc:" + calc(1, 2))
      println("calc2:"+calc(3,4))



   }

   def getCalculator() = (x:Int, y:Int) => x + y

}
