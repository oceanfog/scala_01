package chapter8

/**
 * Created by grkim on 2015-07-22.
 */
object c8_2LocalFunction {
   def main(args: Array[String]) {
      val a = getHello("krk")
      println(a)

   }

   def getHello( name:String ) = {
      def isEmpty() = {
         if (name == "") "empty"
         else name
      }

      "hello " + isEmpty()
   }


}
