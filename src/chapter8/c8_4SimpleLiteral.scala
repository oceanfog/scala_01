package chapter8

/**
 * Created by grkim on 2015-07-21.
 */
object c8_4SimpleLiteral {
   def main(args: Array[String]) {
      val someNumbers = List(-10, -5, 0, 5, 10)

      val newNumbers = someNumbers.filter(x => x > 0)

      for(a <- newNumbers) println(a)

   }

}
