package chapter8

/**
 * Created by grkim on 2015-07-21.
 */
object c8_6Partially {

   def main(args: Array[String]) {
      val someNumbers = List(-10, -5, 0, 5 ,10)
      someNumbers.foreach(println _)

      println("sum123:"+sum(1, 2, 3))
      val a = sum _

      println("sum234:"+a(2, 3, 4))

      println("sumapply:"+a.apply(4,5,6))

      val b = sum(1, _:Int, 3)

      println("b:"+b(10))

   }

   def sum(a:Int, b:Int, c:Int) = a + b + c

}
