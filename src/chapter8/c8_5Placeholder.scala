package chapter8

/**
 * Created by grkim on 2015-07-21.
 */
object c8_5Placeholder {
   def main(args: Array[String]) {
      val f = (_:Int) + (_:Int)
      println(f(4, 5))

   }

}
