/**
 * Created by grkim on 2015-06-24.
 */
class CheckSumAccumulator {

  private var sum = 0

  def add(b:Byte): Unit = {
    sum += b
  }

  def checksum(): Int={
    ~(sum & 0xFF) + 1
  }

}


